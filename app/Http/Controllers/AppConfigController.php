<?php

namespace App\Http\Controllers;

use App\Models\AppConfig;
use App\Http\Requests\StoreAppConfigRequest;
use App\Http\Requests\UpdateAppConfigRequest;

class AppConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAppConfigRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAppConfigRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function show(AppConfig $appConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(AppConfig $appConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAppConfigRequest  $request
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAppConfigRequest $request, AppConfig $appConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppConfig $appConfig)
    {
        //
    }
}
